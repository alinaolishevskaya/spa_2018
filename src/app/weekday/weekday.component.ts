import {Component, OnInit, DoCheck, Input} from '@angular/core';
import {trigger, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-weekday',
  templateUrl: './weekday.component.html',
  styleUrls: ['./weekday.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0}),
        animate(500, style({opacity: 1}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({opacity: 0}))
      ])
    ])
  ]
})
export class WeekdayComponent implements OnInit, DoCheck {

  constructor() {
  }

  activeDay: number;
  timeToAdd = '';
  toDo: '';

  edit: boolean = false;
  toggle: boolean;

  public weekday: Array<any> = [
    {
      name: 'Вск',
      id: 0,
      tasks: []
    },
    {
      name: 'Пн',
      id: 1,
      tasks: [
        {
          time: '00:05',
          message: 'Сделать уборку'
        }
      ]
    },
    {
      name: 'Вт',
      tasks: [
        {
          time: '00:05',
          message: 'Сделать efef'
        }
      ],
      id: 2
    },
    {
      name: 'Ср',
      id: 3,
      tasks: []
    },
    {
      name: 'Чт',
      id: 4,
      tasks: []
    },
    {
      name: 'Пт',
      id: 5,
      tasks: []
    },
    {
      name: 'Сб',
      id: 6,
      tasks: []
    }
  ];

  ngDoCheck() {
    localStorage.setItem('tasks', JSON.stringify(this.weekday));
    this.weekday[+this.activeDay].tasks.sort(function (a, b) {
      if (a.time.replace(':', '') > b.time.replace(':', '')) {
        return 1;
      }
      if (a.time.replace(':', '') < b.time.replace(':', '')) {
        return -1;
      }
      return 0;
    });
  }

  ngOnInit() {
    if (localStorage.getItem('tasks')) {
      this.weekday = JSON.parse(localStorage.getItem('tasks'));
    }
    this.activeDay = new Date().getDay();
  }

  public addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }

  public getTime(day, i) {
    const toDay = new Date().getDay();;
    if (day.time && i <= toDay ) {
      const nowHours = this.addZero(new Date().getHours());
      const nowMin = this.addZero(new Date().getMinutes());
      const nowTime = nowHours.toString() + nowMin.toString();
      const timeTask = day.time.replace(':', '');
      if (+timeTask - +nowTime < 0) {
        return true;
      }
      else {
        false;
      }
    }
  }

  public copyTasks(day) {
    day.tasks = this.weekday[+this.activeDay].tasks;
  }

  public addTask() {
    this.toggle = !this.toggle;
    this.weekday[+this.activeDay].tasks.push({time: this.timeToAdd, message: this.toDo});
  }
}
